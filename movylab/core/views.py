from django.shortcuts import render

# Create your views here.
def Index(request):
    return render(request,"core/index.html")
def Tables(request):
    return render(request,"core/tables.html")
def Register(request):
    return render(request,"core/register.html")
def Login(request):
    return render(request,"core/login.html")
